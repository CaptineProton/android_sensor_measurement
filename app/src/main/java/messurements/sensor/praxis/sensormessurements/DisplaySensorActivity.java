package messurements.sensor.praxis.sensormessurements;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;


/**
 * Created by Sarah on 26.04.2015.
 */
public class DisplaySensorActivity extends Activity implements SensorEventListener{

    // members
    SensorManager mSensorManager;
    Sensor sensor;

    TextView mViewTitle, mXAxisValue, mYAxisValue, mZAxisValue, mXAxisCurrent, mYAxisCurrent, mZAxisCurrent;
    ListView mListView;

    float deltaX, deltaY, deltaZ, maxX, maxY, maxZ = 0;
    float lastX, lastY, lastZ;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        // init view
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sensor_details);
        //init system sensor service
        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        sensor = DeviceSensor.getSensor();

        initView();
        getSensorData();
    }

    @Override
    protected void onResume(){
        super.onResume();
        mSensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }


    // mandatory method (SensorEventListener)
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    // mandatory method (SensorEventListener)
    @Override
    public void onSensorChanged(SensorEvent event){

        // set textViews with values
        cleanSensorState();
        displayCurrentValues();
        displayMaxValues();

        // max. events 3
        int events = event.values.length;

        switch (events){
            case 1:
                deltaX = Math.abs(lastX - event.values[0]);
                break;
            case 2:
                deltaX = Math.abs(lastX - event.values[0]);
                deltaY = Math.abs(lastY - event.values[1]);
                break;
            case 3:
                deltaX = Math.abs(lastX - event.values[0]);
                deltaY = Math.abs(lastY - event.values[1]);
                deltaZ = Math.abs(lastZ - event.values[2]);
                break;
        }


    }

    private void displayCurrentValues(){
        mXAxisCurrent.setText(Float.toString(deltaX));
        mYAxisCurrent.setText(Float.toString(deltaY));
        mZAxisCurrent.setText(Float.toString(deltaZ));
    }

    private void displayMaxValues(){
        if(deltaX > maxX){
            maxX = deltaX;
            mXAxisValue.setText(Float.toString(maxX));
        }
        if(deltaY > maxY){
            maxY = deltaY;
            mYAxisValue.setText(Float.toString(maxY));
        }
        if(deltaZ > maxZ){
            maxZ = deltaZ;
            mZAxisValue.setText(Float.toString(maxZ));
        }
    }

    private void cleanSensorState(){
        mXAxisCurrent.setText("0.0");
        mYAxisCurrent.setText("0.0");
        mZAxisCurrent.setText("0.0");
    }

    private void initView(){

        mViewTitle  = (TextView)findViewById(R.id.textView_SensorType);
        mXAxisValue = (TextView)findViewById(R.id.textView_xAxis_value);
        mYAxisValue = (TextView)findViewById(R.id.textView_yAxis_value);
        mZAxisValue = (TextView)findViewById(R.id.textView_zAxis_value);
        mXAxisCurrent = (TextView)findViewById(R.id.textView_curr_xAxis_value);
        mYAxisCurrent = (TextView)findViewById(R.id.textView_curr_yAxis_value);
        mZAxisCurrent = (TextView)findViewById(R.id.textView_curr_zAxis_value);
        mListView = (ListView)findViewById(R.id.listView_otherValues);

    }

    private void getSensorData(){
        mViewTitle.setText(sensor.getName());
    }
}
