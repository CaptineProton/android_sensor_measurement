package messurements.sensor.praxis.sensormessurements;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SensorActivity extends Activity implements SensorEventListener {

    /*
    *  SensorActivity ist der Einstiegspunkt der Anwendung
    *  (definiert in AndroidManifest.xml)
    */

    // members

    private SensorManager mSensorManager;
    private List<Sensor> mSensorList;
    private ExpandableListAdapter listAdapter;
    private ExpandableListView listView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // app starting point
        // get instance state
        super.onCreate(savedInstanceState);
        // set layout (activity_sensor.xml)
        setContentView(R.layout.activity_sensor);

        // get expandable listview
        listView = (ExpandableListView)findViewById(R.id.expandableListView_1);

        // get system sensor service access
        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        // get all sensors available on the device
        mSensorList = mSensorManager.getSensorList(Sensor.TYPE_ALL);

        prepareListData(mSensorList);

        // create new adapter for ecpandable list (set header and child data)
        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
        // set data to listview item
        listView.setAdapter(listAdapter);

        // set listener events for listview

        // click on group element (header)
        listView.setOnGroupClickListener( new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                // do nothing except expand
                return false;
            }
        });

        // click on expanded listview -> collapse listview
        listView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                // show message text with header text (groupPosition)
                Toast.makeText(getApplicationContext(), listDataHeader.get(groupPosition),
                        Toast.LENGTH_SHORT).show();
            }
        });

        // manage click events on children
        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                // show message
                Toast.makeText(
                        getApplicationContext(),
                        listDataHeader.get(groupPosition)
                                + " : "
                                + listDataChild.get(
                                listDataHeader.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT)
                        .show();

                // check childPosition
                if(childPosition == 6){
                    // childPosition 6 -> "show sensor details" (acts like a button in listview)

                    // Intent shows a new view (DisplaySensorActivity)
                    Intent sensorIntent = new Intent(SensorActivity.this, DisplaySensorActivity.class);
                    // get selected sensor
                    Sensor selectedSensor = mSensorList.get(groupPosition);
                    // set selected sensor
                    DeviceSensor.setSensor(selectedSensor);
                    // start new activity via intent
                    startActivity(sensorIntent);
                }

                return false;
            }
        });

    }


    private void prepareListData(List<Sensor> _sensorList){

        // init lists
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
        int index = 0;
        // add data
        for(Sensor _sensor:_sensorList){

            listDataHeader.add(_sensor.getName());

            List<String> sensorAttributes = new ArrayList<String>();
            sensorAttributes.add("Vender: " + _sensor.getVendor());
            sensorAttributes.add("Version: " + _sensor.getVersion());
            sensorAttributes.add("Power: " + _sensor.getPower());
            // sensorAttributes.add("Max. delay : " +  _sensor.getMaxDelay());
            sensorAttributes.add("Min. delay: " + _sensor.getMinDelay());
            sensorAttributes.add("Max. range: " + _sensor.getMaximumRange());
            //sensorAttributes.add("Reporting mode: " + _sensor.getReportingMode());
            sensorAttributes.add("Resolution: " + _sensor.getResolution());
            sensorAttributes.add("Test this sensor!");

            listDataChild.put(listDataHeader.get(index), sensorAttributes);
            index++;
        }






    }


    @Override
    public void onSensorChanged(SensorEvent event) {

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


}
