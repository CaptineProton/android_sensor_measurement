package messurements.sensor.praxis.sensormessurements;

import android.hardware.Sensor;

/**
 * Created by Sarah on 26.04.2015.
 */
public class DeviceSensor {

    // members
    private static Sensor sensor;

    public static Sensor getSensor(){
        return sensor;
    }

    public static void setSensor(Sensor _sensor){
        sensor = _sensor;
    }

}
